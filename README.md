
This is a simple endpoint that receives and logs data from https://github.com/pjnovas/IoT-NodeMCU webhooks and logs it to a sqlite database

## environment variables

* `CLIENT_SECRET`: Token that should be supplied by the client as a method of authentication
* `DB_FILE`: Defaults to `./db/logs.db`

## start (win)
`SET DEBUG=iot-nodemcu-webhooks:* & npm start`
