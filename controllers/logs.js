

const save = async (req, res, next) => {
  if (!req.body.data) {
    return res.status(400).json({
      message: 'Invalid request',
    });
  }
  try {
    const db = req.app.get('db');
    const data = {
      ':temperature': req.body.data.temperature,
      ':humidity': req.body.data.humidity,
      ':rA': req.body.data.r1,
      ':rB': req.body.data.r2,
      ':createdAt': new Date(),
    };
    await db.run(`INSERT INTO Logs(temperature, humidity, rA, rB, createdAt) 
          VALUES (:temperature, :humidity, :rA, :rB, :createdAt)`, data);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: error.message });
  }
  return next();
};

const getLatest = async (req, res) => {
  const db = req.app.get('db');
  try {
    const log = await db.get('SELECT * FROM Logs ORDER BY id DESC LIMIT 1');
    return res.json(log);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

const getLogs = async (req, res) => {
  const db = req.app.get('db');
  const conditions = [];
  if (req.query.from) {
    conditions.push(`createdAt >= ${req.query.from}`);
  }
  if (req.query.to) {
    conditions.push(`createdAt <= ${req.query.to}`);
  }
  const condition = conditions.length ? `WHERE ${conditions.join(' AND ')}` : '';
  try {
    const logs = await db.all(`SELECT * FROM Logs ${condition} ORDER BY id DESC`);
    const { count } = await db.get(`SELECT COUNT(*) as count FROM Logs ${condition}`);
    return res.json({ items: logs, count });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

module.exports = {
  save,
  getLatest,
  getLogs,
};
