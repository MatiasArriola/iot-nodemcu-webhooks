
const notAuthenticated = res => res.status(401).json({
  message: 'Client token is invalid or missing',
});

const checkToken = (req, res, next) => {
  const authHeader = req.header('Authorization');
  if (!authHeader) {
    return notAuthenticated(res);
  }
  const token = authHeader.split(' ')[1];
  const expectedToken = (process.env.CLIENT_SECRET || '').trim();
  if (token !== expectedToken) {
    return notAuthenticated(res);
  }
  return next();
};

module.exports = {
  checkToken,
};
