-- Up
CREATE TABLE Logs (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    temperature NUMERIC,
    humidity NUMERIC,
    rA BOOLEAN,
    rB BOOLEAN,
    createdAt DATETIME
);

-- Down
DROP TABLE Logs;