const express = require('express');

const router = express.Router();
const packagejson = require('../package.json');
const { checkToken } = require('../middleware/auth');
const logsController = require('../controllers/logs');

router.get('/', (req, res) => {
  const { name, version } = packagejson;
  res.json({
    name,
    version,
  });
});

router.post('/', checkToken, logsController.save, (req, res) => {
  res.json({
    success: true,
  });
});

router.get('/api/v1/logs', logsController.getLogs);

router.get('/api/v1/logs/latest', logsController.getLatest);

module.exports = router;
